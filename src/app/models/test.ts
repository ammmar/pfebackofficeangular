//displayedColumns = ['id', 'theme', 'description', 'Logo', 'date Debut', 'Date Fin ', 'Est Ouvert'];
export class Test {

  idTest: number;
  dateDebut: string;
  dateEvaluation: any;
  dateFin: string;
  description: string;
  etatTest: number;
  logo: any;
  score: number;
  theme: string;
  typeTest: string;
  nomcandidat: string;
  id_candidat: number;
  prenomcandidat: string;

}
