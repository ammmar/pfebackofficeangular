import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtatTestComponent } from './etat-test.component';

describe('EtatTestComponent', () => {
  let component: EtatTestComponent;
  let fixture: ComponentFixture<EtatTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtatTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtatTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
