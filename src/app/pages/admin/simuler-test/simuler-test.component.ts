

import {Component ,OnInit,SimpleChanges } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';
import Swal from "sweetalert2";

import {FormBuilder, FormControl, FormGroup,Validators} from "@angular/forms";
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";

import {MatSelectChange} from '@angular/material';
import {SimulerTestService} from '../../../services/simuler-test.service';
import { timer } from 'rxjs/observable/timer'
import {Observable} from "rxjs";
import {TestService} from '../../../services/test.service';


import { Option, Question, Quiz, QuizConfig } from '../../../models/index';
import {HelperService} from '../../../services/helper.service';

@Component({
  selector: 'app-simuler-test',
  templateUrl: './simuler-test.component.html',
  styleUrls: ['./simuler-test.component.scss']
})
export class SimulerTestComponent implements OnInit {
  currTime: number;
  obsTimer: Observable<number> = timer(1000, 1000);
  idTest:any;
  ListQuestion:[]=[];
  temptest:number;
  Test:any;

  //***************************************quiz info attributes******************************
  quizes: any[];
  quiz: Quiz = new Quiz(null);
  mode = 'quiz';
  quizName: string;
  config: QuizConfig = {
    'allowBack': true,
    'allowReview': true,
    'autoMove': false,  // if true, it will move to next question automatically when answered.
    'duration': 300,  // indicates the time (in secs) in which quiz needs to be completed. 0 means unlimited.
    'pageSize': 1,
    'requiredAll': false,  // indicates if you must answer all the questions before submitting.
    'richText': false,
    'shuffleQuestions': false,
    'shuffleOptions': false,
    'showClock': false,
    'showPager': true,
    'theme': 'none'
  };

  pager = {
    index: 0,
    size: 1,
    count: 1
  };
  timer: any = null;
  startTime: Date;
  endTime: Date;
  ellapsedTime = '00:00';
  duration = '';
  //**************************fin quiz info *********************





  constructor(private activatedRoute:ActivatedRoute, private fb:FormBuilder,private router:Router,private  simulerTestService:SimulerTestService,private testService:TestService) {

    this.idTest=this.activatedRoute.snapshot.params['id'];
    this.afficheMessage("id routed  "+this.idTest);
    this.simulerTestService.getallTestInfo(this.idTest).toPromise().then(reponse=>{
      //this.afficheMessage("result getallInfoTest is "+JSON.stringify(reponse));

      this.ListQuestion=JSON.parse(JSON.stringify(reponse));

      this.temptest = 0;
      for (let i = 0; i < this.ListQuestion.length; i++) {
        let q = this.ListQuestion[i];
        this.temptest = this.temptest + q["tempsQuestion"];
      }
      this.afficheMessage("temps de test est " + this.temptest);

    },error=>{
      this.afficheMessage("error getallInfoTest is "+JSON.stringify(error));

    });

    this.testService.getTestById(this.idTest).toPromise().then(reponse=>{
      this.afficheMessage("test info"+JSON.stringify(reponse));
      this.Test=JSON.parse(JSON.stringify(reponse));
      this.quizName=this.Test.theme;
      //***************************intilisation  of information test **********************************************
      if(this.ListQuestion.length>0) {
     //   let Listquestionquiz: Array<{ id: number, questionId: string ,name:string,isAnswer:boolean}>;
        let Listquestionquiz:any=[];

        for (let i = 0; i < this.ListQuestion.length; i++) {
         // var questionquiz:{id:number,name:string,questionTypeId:number,options:Array<{id:number,questionId:number, name:string,isAnswer:boolean}>;
         let questionquiz:any;
          let q = this.ListQuestion[i];
          let choixs = [];
          choixs = q["choixs"];
          let options :any=[];

     // this.afficheMessage("choix length is question "+i+"is ==>"+choixs.length);

        for (let k = 0; k < choixs.length; k++) {
            let choix = choixs[k];
            let option:any;
            if (choixs[k].reponse === 1) {
              option={"id": choix["idChoix"], "questionId": q["idQuestion"], "name": choixs[k].choix, "isAnswer": true};
             options.push(option);
            } else {
              option={"id": choix["idChoix"], "questionId": q["idQuestion"], "name": choixs[k].choix, "isAnswer": false};
              options.push(option);
            }

         //   this.afficheMessage(" question  "+q["desgnQuestion"] +"  option "+ i+"is=====> "+JSON.stringify(o));
          }
       // this.afficheMessage("type of id question"+typeof q["idQuestion"]);

          questionquiz = {
            "id": q["idQuestion"], "name": q["desgnQuestion"], "questionTypeId": 1,
            "options": options
          }
        //  this.afficheMessage("option data  quiz question is "+JSON.stringify(questionquiz.options));

          Listquestionquiz.push(questionquiz);
        }
//        let data:{id:number,name:string,description:string,config:QuizConfig,questions:Question[]} = {
        let data:any = {
          "id": this.Test.idTest,
          "name": this.Test.theme,
          "description": this.Test.description,
          "config": this.config,
          "questions": Listquestionquiz
        };

      // this.afficheMessage("************Information of new  test information ******** is ====> " + JSON.stringify(data));
       // quiz Info
        this.loadQuiz(data);

      }
      else{
        this.afficheMessage(" no question data for test");
      }
      //******************* fin get all info test


    },error=>{
      this.afficheMessage("error getTestId is "+JSON.stringify(error));

    });




    //this.quiz=new Quiz(data);
  /*  this.pager.count = this.quiz.questions.length;
    this.startTime = new Date();
    this.ellapsedTime = '00:00';
    this.timer = setInterval(() => { this.tick(); }, 1000);
    this.duration = this.parseTime(this.config.duration);
*/

  }

  ngOnInit() {
    this.obsTimer.subscribe(currTime => this.currTime = currTime);

  }




//************** quiz methode **********************
  loadQuiz(data: any) {
  //  console.log("res quiz courant  is"+JSON.stringify(data));
    this.quiz = new Quiz(data);
 this.afficheMessage("quiz question option"+JSON.stringify(this.quiz.questions));
    this.pager.count = this.quiz.questions.length;
    this.afficheMessage("mode quiz is ==>"+this.mode)
    for(let i=0;i<this.quiz.questions.length;i++){
      //console.log("Question i "+JSON.stringify(this.quiz.questions[i])+"\n");
    }
    this.startTime = new Date();
    this.ellapsedTime = '00:00';
    this.timer = setInterval(() => { this.tick(); }, 1000);
    this.duration = this.parseTime(this.config.duration);

  this.mode = 'quiz';
  }

  tick() {
    const now = new Date();
    const diff = (now.getTime() - this.startTime.getTime()) / 1000;
    if (diff >= this.config.duration) {
      this.onSubmit();
    }
    this.ellapsedTime = this.parseTime(diff);
  }

  parseTime(totalSeconds: number) {
    let mins: string | number = Math.floor(totalSeconds / 60);
    let secs: string | number = Math.round(totalSeconds % 60);
    mins = (mins < 10 ? '0' : '') + mins;
    secs = (secs < 10 ? '0' : '') + secs;
    return `${mins}:${secs}`;
  }
  get filteredQuestions() {
    //this.afficheMessage("filtred question is"+JSON.stringify( this.quiz.questions.slice(this.pager.index, this.pager.index + this.pager.size) )+"\n");
    return (this.quiz.questions) ?
      this.quiz.questions.slice(this.pager.index, this.pager.index + this.pager.size) : [];
  }

  onSelect(question: Question, option: Option) {
    if (question.questionTypeId === 1) {
      question.options.forEach((x) => { if (x.id !== option.id) x.selected = false; });
    }

    if (this.config.autoMove) {
      this.goTo(this.pager.index + 1);
    }
  }

  goTo(index: number) {
    if (index >= 0 && index < this.pager.count) {
      this.pager.index = index;
      this.mode = 'quiz';
    }
  }

  isAnswered(question: Question) {
    return question.options.find(x => x.selected) ? 'Answered' : 'Not Answered';
  };

  isCorrect(question: Question) {
    return question.options.every(x => x.selected === x.isAnswer) ? 'correct' : 'wrong';
  };

  onSubmit() {
    let answers = [];
    this.quiz.questions.forEach(x => answers.push({ 'quizId': this.quiz.id, 'questionId': x.id, 'answered': x.answered }));

    // Post your data to the server here. answers contains the questionId and the users' answer.
    console.log("the answer is ====>"+JSON.stringify(answers));
    console.log("question list"+JSON.stringify(this.quiz.questions));
    this.mode = 'result';
  }





  //****************** utile function************************
  afficheMessage(msg:any){
    console.log("********SimulerTestComponent ********"+msg);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('change detected');
  }
}
