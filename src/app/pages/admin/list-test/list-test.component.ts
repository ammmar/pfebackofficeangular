
import { Sort } from '@angular/material';
import { PageEvent } from '@angular/material';
import { fromMatSort, sortRows } from '../../../models/datasource-utils';
import { fromMatPaginator, paginateRows } from '../../../models/datasource-utils';

//https://stackblitz.com/edit/table-like-mat-accordion-2pbhpm?file=app%2Fapp.component.ts


import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import {BehaviorSubject, merge, Observable, of} from 'rxjs';
import { MatTableDataSource, MatPaginator } from '@angular/material';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import { Subject, } from 'rxjs';
import {switchMap } from 'rxjs/operators';

import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
//import{ShowcaseDialogComponent} from "../modal-overlays/dialog/showcase-dialog/showcase-dialog.component"

import { TestService } from 'app/services/test.service';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {AffecterTestComponent} from 'app/Modal/affecter-test/affecter-test.component';
import {QuestionChoixComponent} from '../../../Modal/question-choix/question-choix.component';
@Component({
  selector: 'app-list-test',
  templateUrl: './list-test.component.html',
  styleUrls: ['./list-test.component.scss']
})
export class ListTestComponent implements OnInit, OnDestroy {

public searchText:string;



  //myControls = new FormControl();
  filteredOptions: Observable<string[]>
  ListTest:[]=[];
  myControl = new FormControl();
  //test:FormGroup;
  panelOpenState: boolean=false;
  //dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);

  options:[] = [];
  //  paginator
  @ViewChild(MatSort,{static:false}) sort: MatSort;
  @ViewChild(MatPaginator,{static:false}) paginator: MatPaginator;
  //displayedRows$: Observable<Test[]>;
  totalRows$: Observable<number>;



  constructor(private httpClient: HttpClient,private testService:TestService,private router:Router,private activatedRoute: ActivatedRoute,private changeDetectorRef: ChangeDetectorRef,public dialog: MatDialog) {

    this.testService.getallTest().subscribe((res:any)=>{
     // this.ListTest=res.json();
     //const usersJson: any[] = Array.of(res.json());
     this.ListTest=(JSON.parse(JSON.stringify(res)));

      //console.log("Response test result is ",res);
      //this.ListTest=response;
    console.log("list test "+this.ListTest.length);
  for(let i=0; i<this.ListTest.length; i++){
      const t=this.ListTest[i];
      console.log("test["+i+"]"+   t['idTest']+t['description']);
    }


    }

    );

  }





  ngOnInit() {
    const sortEvents$: Observable<Sort> = fromMatSort(this.sort);
    const pageEvents$: Observable<PageEvent> = fromMatPaginator(this.paginator);

    const rows$ = of(this.ListTest);


    this.totalRows$ = rows$.pipe(map(rows => rows.length));
    console.log("total rows is "+JSON.stringify(this.totalRows$));
    //this.displayedRows$ = rows$.pipe(sortRows(sortEvents$), paginateRows(pageEvents$));

  }
  deleteDilaog(idTest:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Voulez vous supprimer!',
      cancelButtonText: 'Non!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {
        this.testService.deleteTest(idTest).toPromise().then(reponse=>{
          console.log("result de suprression"+JSON.stringify(reponse));
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        },error=>{
          swalWithBootstrapButtons.fire(
            'Probleme',
            'Probelem de suppression ',
            'warning'
          )
        });

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Anuller',
          'votre test est en cours  :)',
          'error'
        )
      }
    })
  }

//********* dialog
  openDialogAffecter(id_test:any,name:any): void {
    //this.afficheMsg("Dialog  id question"+id_question+" name question"+namequestion);
    const dialogRef = this.dialog.open(AffecterTestComponent, {
      width: '700px',
      height:'500px',
      data:{id_test:id_test,name:name}

    });
    this.afficheMsg("AffecterDialog open");

    dialogRef.afterClosed().subscribe(res => {
      console.log("AffecterDialog closed");

    });
  }

  navAddTest() {
    this.router.navigate(['/test/add']);
  }
  navQuestionsTest(){
    this.router.navigate(['/question-test/{id}'])
  }
  afficheMsg(msg:any){
    console.log("*****AffecterTestComponents******"+msg);
  }
  ngOnDestroy() {

  }
}



