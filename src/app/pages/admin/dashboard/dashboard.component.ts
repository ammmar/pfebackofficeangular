import { Component, OnInit } from '@angular/core';
import  * as Chart from 'chart.js';
import { ChartDataSets, ChartOptions , ChartType} from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { SingleDataSet,  monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import {StatService} from '../../../services/stat.service';

//https://www.positronx.io/angular-chart-js-tutorial-with-ng2-charts-examples/
@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit{




  //*test attributes

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [['Réussite'], ['non effectuté'], 'echoué'];
  public pieChartData: SingleDataSet = [30, 50, 20];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  Stat:any;
  nbquestion:any;
  nbTest:any;
  nbCandidat:any;



  constructor(private statService:StatService) {

    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();

    this.statService.getstatInfo().toPromise().then(reponse=>{
      this.afficheMsg("reponse"+JSON.stringify(reponse));
      this.Stat=JSON.parse(JSON.stringify(reponse));
      this.afficheMsg("Nombre de test"+this.Stat["nombreTest"]);
      this.afficheMsg("Nombre de question"+this.Stat["nombreQuestion"]);
      this.nbquestion=this.Stat["nombreQuestion"];
      this.nbTest=this.Stat["nombreTest"];
      this.nbCandidat=this.Stat["nombreCandiat"];

    },error=>{
      this.afficheMsg("error+"+JSON.stringify(error));
    });
  }
afficheMsg(msg:any){
    console.log("***********Dashbord********              "+msg)
}


  //*candiat attributes
  lineChartData: ChartDataSets[] = [
    { data: [85, 72, 78, 75, 77, 75,74,80,75,73,84,70], label: 'Candidature' },
  ];

  lineChartLabels: Label[] = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin','Juillet','aout','septembre','octobre','novembre','décembre'];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line'

    ngOnInit() {


    }


}
