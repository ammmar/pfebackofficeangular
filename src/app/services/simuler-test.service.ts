import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient, HttpParams} from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SimulerTestService {
  private baseUrl = 'http://localhost:9093/auth/simulerTest';

  constructor(private httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });
  }
  getallTestInfo(id:any):Observable<Object>{
    return this.httpClient.get(`${this.baseUrl}/getQuestionByIdTest/${id}`);
  }
}
