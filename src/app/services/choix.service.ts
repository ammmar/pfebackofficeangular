import { Injectable } from '@angular/core';

import {HttpHeaders, HttpClient, HttpParams} from "@angular/common/http";
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ChoixService {

  private baseUrl = 'http://localhost:9093/auth/choix/';

  constructor(private httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });
  }

  creerChoixByIdQuestion(choix:any,id:any):Observable<Object>{
    return this.httpClient.put(`${this.baseUrl}creerChoix/${id}`,choix);
  }

  EnregistrerStateQuestionChoix(choix:any,id:any):Observable<Object>{
    return this.httpClient.put(`${this.baseUrl}EnregistrerStateQuestionChoix/${id}`,choix);
  }
}
