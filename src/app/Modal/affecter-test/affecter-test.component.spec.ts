import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffecterTestComponent } from './affecter-test.component';

describe('AffecterTestComponent', () => {
  let component: AffecterTestComponent;
  let fixture: ComponentFixture<AffecterTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffecterTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffecterTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
