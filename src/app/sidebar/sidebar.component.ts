import { Component, OnInit } from '@angular/core';


export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    { path: '/dashboard',     title: 'Dashboard',         icon:'nc-bank',       class: '' },
    { path: '/user',          title: 'User Profile',      icon:'nc-settings-gear-65',  class: '' },
    { path: '/candidat',       title: 'Candidat',    icon:'nc-single-02',  class: '' },
    { path: '/test',          title: 'Test',     icon:'nc-tile-56',    class: '' },
    { path: '/etatTest',         title: 'EtatTest',             icon:'nc-bell-55',    class: '' },
    { path: '/icons',         title: 'ResultatTest',             icon:'nc-zoom-split',    class: '' },
    { path: '/icons',         title: 'Utlisateur',             icon:'nc-single-02',    class: '' },




];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
}
